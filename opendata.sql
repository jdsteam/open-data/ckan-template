--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.13
-- Dumped by pg_dump version 10.10 (Ubuntu 10.10-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: activity; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.activity (
    id text NOT NULL,
    "timestamp" timestamp without time zone,
    user_id text,
    object_id text,
    revision_id text,
    activity_type text,
    data text
);


--
-- Name: activity_detail; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.activity_detail (
    id text NOT NULL,
    activity_id text,
    object_id text,
    object_type text,
    activity_type text,
    data text
);


--
-- Name: authorization_group; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.authorization_group (
    id text NOT NULL,
    name text,
    created timestamp without time zone
);


--
-- Name: authorization_group_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.authorization_group_user (
    authorization_group_id text NOT NULL,
    user_id text NOT NULL,
    id text NOT NULL
);


--
-- Name: dashboard; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dashboard (
    user_id text NOT NULL,
    activity_stream_last_viewed timestamp without time zone NOT NULL,
    email_last_sent timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL
);


--
-- Name: group; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."group" (
    id text NOT NULL,
    name text NOT NULL,
    title text,
    description text,
    created timestamp without time zone,
    state text,
    revision_id text,
    type text NOT NULL,
    approval_status text,
    image_url text,
    is_organization boolean DEFAULT false
);


--
-- Name: group_extra; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.group_extra (
    id text NOT NULL,
    group_id text,
    key text,
    value text,
    state text,
    revision_id text
);


--
-- Name: group_extra_revision; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.group_extra_revision (
    id text NOT NULL,
    group_id text,
    key text,
    value text,
    state text,
    revision_id text NOT NULL,
    continuity_id text,
    expired_id text,
    revision_timestamp timestamp without time zone,
    expired_timestamp timestamp without time zone,
    current boolean
);


--
-- Name: group_revision; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.group_revision (
    id text NOT NULL,
    name text NOT NULL,
    title text,
    description text,
    created timestamp without time zone,
    state text,
    revision_id text NOT NULL,
    continuity_id text,
    expired_id text,
    revision_timestamp timestamp without time zone,
    expired_timestamp timestamp without time zone,
    current boolean,
    type text NOT NULL,
    approval_status text,
    image_url text,
    is_organization boolean DEFAULT false
);


--
-- Name: member; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.member (
    id text NOT NULL,
    table_id text NOT NULL,
    group_id text,
    state text,
    revision_id text,
    table_name text NOT NULL,
    capacity text NOT NULL
);


--
-- Name: member_revision; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.member_revision (
    id text NOT NULL,
    table_id text NOT NULL,
    group_id text,
    state text,
    revision_id text NOT NULL,
    continuity_id text,
    expired_id text,
    revision_timestamp timestamp without time zone,
    expired_timestamp timestamp without time zone,
    current boolean,
    table_name text NOT NULL,
    capacity text NOT NULL
);


--
-- Name: migrate_version; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.migrate_version (
    repository_id character varying(250) NOT NULL,
    repository_path text,
    version integer
);


--
-- Name: package; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.package (
    id text NOT NULL,
    name character varying(100) NOT NULL,
    title text,
    version character varying(100),
    url text,
    notes text,
    license_id text,
    revision_id text,
    author text,
    author_email text,
    maintainer text,
    maintainer_email text,
    state text,
    type text,
    owner_org text,
    private boolean DEFAULT false,
    metadata_modified timestamp without time zone,
    creator_user_id text,
    metadata_created timestamp without time zone
);


--
-- Name: package_extra; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.package_extra (
    id text NOT NULL,
    package_id text,
    key text,
    value text,
    revision_id text,
    state text
);


--
-- Name: package_extra_revision; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.package_extra_revision (
    id text NOT NULL,
    package_id text,
    key text,
    value text,
    revision_id text NOT NULL,
    continuity_id text,
    state text,
    expired_id text,
    revision_timestamp timestamp without time zone,
    expired_timestamp timestamp without time zone,
    current boolean
);


--
-- Name: package_relationship; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.package_relationship (
    id text NOT NULL,
    subject_package_id text,
    object_package_id text,
    type text,
    comment text,
    revision_id text,
    state text
);


--
-- Name: package_relationship_revision; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.package_relationship_revision (
    id text NOT NULL,
    subject_package_id text,
    object_package_id text,
    type text,
    comment text,
    revision_id text NOT NULL,
    continuity_id text,
    state text,
    expired_id text,
    revision_timestamp timestamp without time zone,
    expired_timestamp timestamp without time zone,
    current boolean
);


--
-- Name: package_revision; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.package_revision (
    id text NOT NULL,
    name character varying(100) NOT NULL,
    title text,
    version character varying(100),
    url text,
    notes text,
    license_id text,
    revision_id text NOT NULL,
    continuity_id text,
    author text,
    author_email text,
    maintainer text,
    maintainer_email text,
    state text,
    expired_id text,
    revision_timestamp timestamp without time zone,
    expired_timestamp timestamp without time zone,
    current boolean,
    type text,
    owner_org text,
    private boolean DEFAULT false,
    metadata_modified timestamp without time zone,
    creator_user_id text,
    metadata_created timestamp without time zone
);


--
-- Name: package_tag; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.package_tag (
    id text NOT NULL,
    package_id text,
    tag_id text,
    revision_id text,
    state text
);


--
-- Name: package_tag_revision; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.package_tag_revision (
    id text NOT NULL,
    package_id text,
    tag_id text,
    revision_id text NOT NULL,
    continuity_id text,
    state text,
    expired_id text,
    revision_timestamp timestamp without time zone,
    expired_timestamp timestamp without time zone,
    current boolean
);


--
-- Name: rating; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.rating (
    id text NOT NULL,
    user_id text,
    user_ip_address text,
    package_id text,
    rating double precision,
    created timestamp without time zone
);


--
-- Name: resource; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.resource (
    id text NOT NULL,
    url text NOT NULL,
    format text,
    description text,
    "position" integer,
    revision_id text,
    hash text,
    state text,
    extras text,
    name text,
    resource_type text,
    mimetype text,
    mimetype_inner text,
    size bigint,
    last_modified timestamp without time zone,
    cache_url text,
    cache_last_updated timestamp without time zone,
    webstore_url text,
    webstore_last_updated timestamp without time zone,
    created timestamp without time zone,
    url_type text,
    package_id text DEFAULT ''::text NOT NULL
);


--
-- Name: resource_revision; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.resource_revision (
    id text NOT NULL,
    url text NOT NULL,
    format text,
    description text,
    "position" integer,
    revision_id text NOT NULL,
    continuity_id text,
    hash text,
    state text,
    extras text,
    expired_id text,
    revision_timestamp timestamp without time zone,
    expired_timestamp timestamp without time zone,
    current boolean,
    name text,
    resource_type text,
    mimetype text,
    mimetype_inner text,
    size bigint,
    last_modified timestamp without time zone,
    cache_url text,
    cache_last_updated timestamp without time zone,
    webstore_url text,
    webstore_last_updated timestamp without time zone,
    created timestamp without time zone,
    url_type text,
    package_id text DEFAULT ''::text NOT NULL
);


--
-- Name: resource_view; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.resource_view (
    id text NOT NULL,
    resource_id text,
    title text,
    description text,
    view_type text NOT NULL,
    "order" integer NOT NULL,
    config text
);


--
-- Name: revision; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.revision (
    id text NOT NULL,
    "timestamp" timestamp without time zone,
    author character varying(200),
    message text,
    state text,
    approved_timestamp timestamp without time zone
);


--
-- Name: system_info; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.system_info (
    id integer NOT NULL,
    key character varying(100) NOT NULL,
    value text,
    revision_id text,
    state text DEFAULT 'active'::text NOT NULL
);


--
-- Name: system_info_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.system_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: system_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.system_info_id_seq OWNED BY public.system_info.id;


--
-- Name: system_info_revision; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.system_info_revision (
    id integer NOT NULL,
    key character varying(100) NOT NULL,
    value text,
    revision_id text NOT NULL,
    continuity_id integer,
    state text DEFAULT 'active'::text NOT NULL,
    expired_id text,
    revision_timestamp timestamp without time zone,
    expired_timestamp timestamp without time zone,
    current boolean
);


--
-- Name: tag; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tag (
    id text NOT NULL,
    name character varying(100) NOT NULL,
    vocabulary_id character varying(100)
);


--
-- Name: task_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.task_status (
    id text NOT NULL,
    entity_id text NOT NULL,
    entity_type text NOT NULL,
    task_type text NOT NULL,
    key text NOT NULL,
    value text NOT NULL,
    state text,
    error text,
    last_updated timestamp without time zone
);


--
-- Name: term_translation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.term_translation (
    term text NOT NULL,
    term_translation text NOT NULL,
    lang_code text NOT NULL
);


--
-- Name: tracking_raw; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tracking_raw (
    user_key character varying(100) NOT NULL,
    url text NOT NULL,
    tracking_type character varying(10) NOT NULL,
    access_timestamp timestamp without time zone DEFAULT now()
);


--
-- Name: tracking_summary; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tracking_summary (
    url text NOT NULL,
    package_id text,
    tracking_type character varying(10) NOT NULL,
    count integer NOT NULL,
    running_total integer DEFAULT 0 NOT NULL,
    recent_views integer DEFAULT 0 NOT NULL,
    tracking_date date
);


--
-- Name: user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."user" (
    id text NOT NULL,
    name text NOT NULL,
    apikey text,
    created timestamp without time zone,
    about text,
    password text,
    fullname text,
    email text,
    reset_key text,
    sysadmin boolean DEFAULT false,
    activity_streams_email_notifications boolean DEFAULT false,
    state text DEFAULT 'active'::text NOT NULL
);


--
-- Name: user_following_dataset; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_following_dataset (
    follower_id text NOT NULL,
    object_id text NOT NULL,
    datetime timestamp without time zone NOT NULL
);


--
-- Name: user_following_group; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_following_group (
    follower_id text NOT NULL,
    object_id text NOT NULL,
    datetime timestamp without time zone NOT NULL
);


--
-- Name: user_following_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_following_user (
    follower_id text NOT NULL,
    object_id text NOT NULL,
    datetime timestamp without time zone NOT NULL
);


--
-- Name: vocabulary; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.vocabulary (
    id text NOT NULL,
    name character varying(100) NOT NULL
);


--
-- Name: system_info id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_info ALTER COLUMN id SET DEFAULT nextval('public.system_info_id_seq'::regclass);


--
-- Data for Name: activity; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.activity (id, "timestamp", user_id, object_id, revision_id, activity_type, data) FROM stdin;
f9e3345a-e10f-430f-a0e9-d137b4552399	2019-11-04 09:10:03.5657	ea6cae7f-3fac-4d7b-b59b-64df33a71528	ea6cae7f-3fac-4d7b-b59b-64df33a71528	\N	new user	\N
59e3ebec-2a8d-45a7-8c5c-67024a5fd322	2019-11-04 09:44:48.991544	ea6cae7f-3fac-4d7b-b59b-64df33a71528	eb657e16-c5a5-409c-8d85-74712da3918a	05e4fbf1-95b5-4267-93c5-38b53c38d250	new group	{"group": {"description": "", "title": "Kesenian", "created": "2019-11-04T16:44:48.977448", "approval_status": "approved", "is_organization": false, "state": "active", "image_url": "2019-11-04-094448.96051520141223-193733.151747iconsart.png", "revision_id": "05e4fbf1-95b5-4267-93c5-38b53c38d250", "type": "group", "id": "eb657e16-c5a5-409c-8d85-74712da3918a", "name": "kesenian"}}
49d51caa-9b0a-41b9-8bdd-3aff9699dfd4	2019-11-04 09:54:48.376263	ea6cae7f-3fac-4d7b-b59b-64df33a71528	ce621bdc-853b-4c28-af24-f8203f4ea18b	618b8421-786d-42e4-b197-a804909221c8	new group	{"group": {"description": "", "title": "Basemap", "created": "2019-11-04T16:54:48.365423", "approval_status": "approved", "is_organization": false, "state": "active", "image_url": "2019-11-04-095448.33092720180402-210608.839199basemap.png", "revision_id": "618b8421-786d-42e4-b197-a804909221c8", "type": "group", "id": "ce621bdc-853b-4c28-af24-f8203f4ea18b", "name": "basemap"}}
0838f56e-6780-48f9-bee9-0279a510c142	2019-11-04 09:56:18.412584	ea6cae7f-3fac-4d7b-b59b-64df33a71528	ce621bdc-853b-4c28-af24-f8203f4ea18b	410d242b-eeea-4e58-ad95-9972a93fd09d	changed group	{"group": {"description": "", "title": "Basemap", "created": "2019-11-04T16:54:48.365423", "approval_status": "approved", "is_organization": false, "state": "active", "image_url": "2019-11-04-095618.40569220180402-210608.839199basemap.png", "revision_id": "618b8421-786d-42e4-b197-a804909221c8", "type": "group", "id": "ce621bdc-853b-4c28-af24-f8203f4ea18b", "name": "basemap"}}
24d948e3-69fe-414b-a04e-aa310a6645e2	2019-11-04 09:56:47.888338	ea6cae7f-3fac-4d7b-b59b-64df33a71528	cb7600e5-0667-4eed-8519-636a768b832f	78b16dcd-0a9a-4210-add2-755ba6ad717d	new group	{"group": {"description": "", "title": "Boundaries", "created": "2019-11-04T16:56:47.873358", "approval_status": "approved", "is_organization": false, "state": "active", "image_url": "2019-11-04-095647.84197720180402-210631.609197boundaries.png", "revision_id": "78b16dcd-0a9a-4210-add2-755ba6ad717d", "type": "group", "id": "cb7600e5-0667-4eed-8519-636a768b832f", "name": "boundaries"}}
0883b0da-d227-4963-b0de-95cc5be7580f	2019-11-04 09:57:03.390625	ea6cae7f-3fac-4d7b-b59b-64df33a71528	ac1dc7a8-7fe6-4ebc-b6b9-41b38845d7f7	6057c8b7-18a0-481d-bd88-214fe3fbbba5	new group	{"group": {"description": "", "title": "Finansial", "created": "2019-11-04T16:57:03.382296", "approval_status": "approved", "is_organization": false, "state": "active", "image_url": "2019-11-04-095703.37248020141223-193756.990691iconsbudget.png", "revision_id": "6057c8b7-18a0-481d-bd88-214fe3fbbba5", "type": "group", "id": "ac1dc7a8-7fe6-4ebc-b6b9-41b38845d7f7", "name": "finansial"}}
fefebbff-56f2-4d12-844d-991b965a1514	2019-11-04 09:57:19.11697	ea6cae7f-3fac-4d7b-b59b-64df33a71528	3bada3b5-c6b2-47c9-ac89-f5eaa2232804	ea82b0e2-d4ec-44f3-b7c4-62cbdadeb1af	new group	{"group": {"description": "", "title": "Ekonomi", "created": "2019-11-04T16:57:19.106237", "approval_status": "approved", "is_organization": false, "state": "active", "image_url": "2019-11-04-095719.09218320141223-193813.665606iconseconomy.png", "revision_id": "ea82b0e2-d4ec-44f3-b7c4-62cbdadeb1af", "type": "group", "id": "3bada3b5-c6b2-47c9-ac89-f5eaa2232804", "name": "ekonomi"}}
d3af99c6-9366-4c4d-8173-9625a0a68653	2019-11-04 09:57:32.175851	ea6cae7f-3fac-4d7b-b59b-64df33a71528	2f4171f4-545d-4ad5-a0a0-d4aa5745fe68	41251724-13bb-4dd0-9693-241e3bab4ecd	new group	{"group": {"description": "", "title": "Pendidikan", "created": "2019-11-04T16:57:32.167765", "approval_status": "approved", "is_organization": false, "state": "active", "image_url": "2019-11-04-095732.15739120141223-194239.846305iconseducation.png", "revision_id": "41251724-13bb-4dd0-9693-241e3bab4ecd", "type": "group", "id": "2f4171f4-545d-4ad5-a0a0-d4aa5745fe68", "name": "pendidikan"}}
dd2fd564-727b-46ac-a267-8ea5a949a131	2019-11-04 09:57:45.693769	ea6cae7f-3fac-4d7b-b59b-64df33a71528	e1024296-5068-4f29-bcd8-d137fb6cde61	77326290-28b7-44d1-bc54-17d5c6941d4b	new group	{"group": {"description": "", "title": "Politik", "created": "2019-11-04T16:57:45.685750", "approval_status": "approved", "is_organization": false, "state": "active", "image_url": "2019-11-04-095745.67095620141223-193842.916844iconselections.png", "revision_id": "77326290-28b7-44d1-bc54-17d5c6941d4b", "type": "group", "id": "e1024296-5068-4f29-bcd8-d137fb6cde61", "name": "politik"}}
7b641231-7ad7-41b5-8514-e6a4bc1a922e	2019-11-04 09:58:08.361008	ea6cae7f-3fac-4d7b-b59b-64df33a71528	2c0f9867-57a3-49d4-a672-fbf9beb17bae	f6c82422-3d68-4ea7-98a7-1acd83e78da6	new group	{"group": {"description": "", "title": "Lingkungan Hidup", "created": "2019-11-04T16:58:08.353549", "approval_status": "approved", "is_organization": false, "state": "active", "image_url": "2019-11-04-095808.34064820141223-194254.915092iconsenvironment.png", "revision_id": "f6c82422-3d68-4ea7-98a7-1acd83e78da6", "type": "group", "id": "2c0f9867-57a3-49d4-a672-fbf9beb17bae", "name": "lingkungan-hidup"}}
4da59a77-0b67-4cad-b66d-02255e0e4228	2019-11-04 09:59:01.254118	ea6cae7f-3fac-4d7b-b59b-64df33a71528	fb0c05e9-d9ce-462e-8c4f-e2ad911f41d8	8a902bd1-eba7-47b1-80ab-6427d26f1e35	new group	{"group": {"description": "", "title": "Pangan", "created": "2019-11-04T16:59:01.245702", "approval_status": "approved", "is_organization": false, "state": "active", "image_url": "2019-11-04-095901.23179620141223-194334.639060iconsfood.png", "revision_id": "8a902bd1-eba7-47b1-80ab-6427d26f1e35", "type": "group", "id": "fb0c05e9-d9ce-462e-8c4f-e2ad911f41d8", "name": "pangan"}}
2f09beb3-daa3-460a-b352-6a77d845c4cd	2019-11-04 09:59:15.953883	ea6cae7f-3fac-4d7b-b59b-64df33a71528	2cd00e37-12b3-4e09-a4d5-00b1a2c3c68d	09909a8f-ab3a-4258-bf11-8612b3043289	new group	{"group": {"description": "", "title": "Kesehatan", "created": "2019-11-04T16:59:15.945008", "approval_status": "approved", "is_organization": false, "state": "active", "image_url": "2019-11-04-095915.93053820141223-193813.665606iconseconomy.png", "revision_id": "09909a8f-ab3a-4258-bf11-8612b3043289", "type": "group", "id": "2cd00e37-12b3-4e09-a4d5-00b1a2c3c68d", "name": "kesehatan"}}
cfa72a16-05c1-4b4c-99ed-86e1d63c1c96	2019-11-04 09:59:51.188245	ea6cae7f-3fac-4d7b-b59b-64df33a71528	fc6edd36-7312-47e8-81e6-b149724ca8f2	2faf179c-a5c4-4bd3-bfcb-007f3cf80ef9	new group	{"group": {"description": "", "title": "Pertamanan", "created": "2019-11-04T16:59:51.179572", "approval_status": "approved", "is_organization": false, "state": "active", "image_url": "2019-11-04-095951.16839620141223-194351.335866iconsparks.png", "revision_id": "2faf179c-a5c4-4bd3-bfcb-007f3cf80ef9", "type": "group", "id": "fc6edd36-7312-47e8-81e6-b149724ca8f2", "name": "pertamanan"}}
cb4582d9-b3ae-4f14-a520-271e8f3204f8	2019-11-04 10:00:11.263895	ea6cae7f-3fac-4d7b-b59b-64df33a71528	3b10736c-2100-4d15-a5ad-0bd35b2871fd	7e785b9b-f384-4995-b7d6-e4dd15d2e144	new group	{"group": {"description": "", "title": "Tata Ruang", "created": "2019-11-04T17:00:11.237771", "approval_status": "approved", "is_organization": false, "state": "active", "image_url": "2019-11-04-100011.22152820141223-194423.154603iconsplanning.png", "revision_id": "7e785b9b-f384-4995-b7d6-e4dd15d2e144", "type": "group", "id": "3b10736c-2100-4d15-a5ad-0bd35b2871fd", "name": "tata-ruang"}}
601f5062-4d9e-45a5-88c7-75d52f31d2fb	2019-11-04 10:00:29.857878	ea6cae7f-3fac-4d7b-b59b-64df33a71528	09818b3a-567b-402a-8374-e52053beff27	b75a4b6e-f830-48a8-a589-02e963ce7756	new group	{"group": {"description": "", "title": "Hukum", "created": "2019-11-04T17:00:29.850100", "approval_status": "approved", "is_organization": false, "state": "active", "image_url": "2019-11-04-100029.83763820141223-194440.888468iconspublicsafety.png", "revision_id": "b75a4b6e-f830-48a8-a589-02e963ce7756", "type": "group", "id": "09818b3a-567b-402a-8374-e52053beff27", "name": "hukum"}}
24209764-f1d9-42e8-91c4-d20ccc162827	2019-11-04 10:01:26.235425	ea6cae7f-3fac-4d7b-b59b-64df33a71528	80238a3e-9931-4e85-af8c-409e5548bafd	18c01c9d-ddb3-4896-8c84-05f044b3f323	new group	{"group": {"description": "", "title": "Lainnya", "created": "2019-11-04T17:01:26.225478", "approval_status": "approved", "is_organization": false, "state": "active", "image_url": "2019-11-04-100126.19773520141223-194512.006490iconsdocument.png", "revision_id": "18c01c9d-ddb3-4896-8c84-05f044b3f323", "type": "group", "id": "80238a3e-9931-4e85-af8c-409e5548bafd", "name": "lainnya"}}
870e44c0-dbc1-485c-a07b-0978cb17f8f9	2019-11-04 10:00:46.376969	ea6cae7f-3fac-4d7b-b59b-64df33a71528	b27694bb-adce-4643-81a0-d956498b8fae	9a175ac9-4216-45ff-b983-ca720c94694c	new group	{"group": {"description": "", "title": "Industri", "created": "2019-11-04T17:00:46.369069", "approval_status": "approved", "is_organization": false, "state": "active", "image_url": "2019-11-04-100046.35703220141223-194406.761461iconsrealestate.png", "revision_id": "9a175ac9-4216-45ff-b983-ca720c94694c", "type": "group", "id": "b27694bb-adce-4643-81a0-d956498b8fae", "name": "industri"}}
c7d033cf-2536-44db-b217-dd9708cffe8b	2019-11-04 10:01:13.249226	ea6cae7f-3fac-4d7b-b59b-64df33a71528	3fc4e7fb-ac98-4466-b99b-c0d4864aa6f9	79e40da0-ae4e-4d0f-acca-ce3bc25265da	new group	{"group": {"description": "", "title": "Transportasi", "created": "2019-11-04T17:01:13.242429", "approval_status": "approved", "is_organization": false, "state": "active", "image_url": "2019-11-04-100113.22880920141223-194455.342914iconstransportation.png", "revision_id": "79e40da0-ae4e-4d0f-acca-ce3bc25265da", "type": "group", "id": "3fc4e7fb-ac98-4466-b99b-c0d4864aa6f9", "name": "transportasi"}}
\.


--
-- Data for Name: activity_detail; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.activity_detail (id, activity_id, object_id, object_type, activity_type, data) FROM stdin;
\.


--
-- Data for Name: authorization_group; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.authorization_group (id, name, created) FROM stdin;
\.


--
-- Data for Name: authorization_group_user; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.authorization_group_user (authorization_group_id, user_id, id) FROM stdin;
\.


--
-- Data for Name: dashboard; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.dashboard (user_id, activity_stream_last_viewed, email_last_sent) FROM stdin;
ea6cae7f-3fac-4d7b-b59b-64df33a71528	2019-11-04 09:48:50.947927	2019-11-04 09:10:03.585671
\.


--
-- Data for Name: group; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public."group" (id, name, title, description, created, state, revision_id, type, approval_status, image_url, is_organization) FROM stdin;
eb657e16-c5a5-409c-8d85-74712da3918a	kesenian	Kesenian		2019-11-04 16:44:48.977448	active	05e4fbf1-95b5-4267-93c5-38b53c38d250	group	approved	2019-11-04-094448.96051520141223-193733.151747iconsart.png	f
ce621bdc-853b-4c28-af24-f8203f4ea18b	basemap	Basemap		2019-11-04 16:54:48.365423	active	410d242b-eeea-4e58-ad95-9972a93fd09d	group	approved	2019-11-04-095618.40569220180402-210608.839199basemap.png	f
cb7600e5-0667-4eed-8519-636a768b832f	boundaries	Boundaries		2019-11-04 16:56:47.873358	active	78b16dcd-0a9a-4210-add2-755ba6ad717d	group	approved	2019-11-04-095647.84197720180402-210631.609197boundaries.png	f
ac1dc7a8-7fe6-4ebc-b6b9-41b38845d7f7	finansial	Finansial		2019-11-04 16:57:03.382296	active	6057c8b7-18a0-481d-bd88-214fe3fbbba5	group	approved	2019-11-04-095703.37248020141223-193756.990691iconsbudget.png	f
3bada3b5-c6b2-47c9-ac89-f5eaa2232804	ekonomi	Ekonomi		2019-11-04 16:57:19.106237	active	ea82b0e2-d4ec-44f3-b7c4-62cbdadeb1af	group	approved	2019-11-04-095719.09218320141223-193813.665606iconseconomy.png	f
2f4171f4-545d-4ad5-a0a0-d4aa5745fe68	pendidikan	Pendidikan		2019-11-04 16:57:32.167765	active	41251724-13bb-4dd0-9693-241e3bab4ecd	group	approved	2019-11-04-095732.15739120141223-194239.846305iconseducation.png	f
e1024296-5068-4f29-bcd8-d137fb6cde61	politik	Politik		2019-11-04 16:57:45.68575	active	77326290-28b7-44d1-bc54-17d5c6941d4b	group	approved	2019-11-04-095745.67095620141223-193842.916844iconselections.png	f
2c0f9867-57a3-49d4-a672-fbf9beb17bae	lingkungan-hidup	Lingkungan Hidup		2019-11-04 16:58:08.353549	active	f6c82422-3d68-4ea7-98a7-1acd83e78da6	group	approved	2019-11-04-095808.34064820141223-194254.915092iconsenvironment.png	f
fb0c05e9-d9ce-462e-8c4f-e2ad911f41d8	pangan	Pangan		2019-11-04 16:59:01.245702	active	8a902bd1-eba7-47b1-80ab-6427d26f1e35	group	approved	2019-11-04-095901.23179620141223-194334.639060iconsfood.png	f
2cd00e37-12b3-4e09-a4d5-00b1a2c3c68d	kesehatan	Kesehatan		2019-11-04 16:59:15.945008	active	09909a8f-ab3a-4258-bf11-8612b3043289	group	approved	2019-11-04-095915.93053820141223-193813.665606iconseconomy.png	f
fc6edd36-7312-47e8-81e6-b149724ca8f2	pertamanan	Pertamanan		2019-11-04 16:59:51.179572	active	2faf179c-a5c4-4bd3-bfcb-007f3cf80ef9	group	approved	2019-11-04-095951.16839620141223-194351.335866iconsparks.png	f
3b10736c-2100-4d15-a5ad-0bd35b2871fd	tata-ruang	Tata Ruang		2019-11-04 17:00:11.237771	active	7e785b9b-f384-4995-b7d6-e4dd15d2e144	group	approved	2019-11-04-100011.22152820141223-194423.154603iconsplanning.png	f
09818b3a-567b-402a-8374-e52053beff27	hukum	Hukum		2019-11-04 17:00:29.8501	active	b75a4b6e-f830-48a8-a589-02e963ce7756	group	approved	2019-11-04-100029.83763820141223-194440.888468iconspublicsafety.png	f
b27694bb-adce-4643-81a0-d956498b8fae	industri	Industri		2019-11-04 17:00:46.369069	active	9a175ac9-4216-45ff-b983-ca720c94694c	group	approved	2019-11-04-100046.35703220141223-194406.761461iconsrealestate.png	f
3fc4e7fb-ac98-4466-b99b-c0d4864aa6f9	transportasi	Transportasi		2019-11-04 17:01:13.242429	active	79e40da0-ae4e-4d0f-acca-ce3bc25265da	group	approved	2019-11-04-100113.22880920141223-194455.342914iconstransportation.png	f
80238a3e-9931-4e85-af8c-409e5548bafd	lainnya	Lainnya		2019-11-04 17:01:26.225478	active	18c01c9d-ddb3-4896-8c84-05f044b3f323	group	approved	2019-11-04-100126.19773520141223-194512.006490iconsdocument.png	f
\.


--
-- Data for Name: group_extra; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.group_extra (id, group_id, key, value, state, revision_id) FROM stdin;
\.


--
-- Data for Name: group_extra_revision; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.group_extra_revision (id, group_id, key, value, state, revision_id, continuity_id, expired_id, revision_timestamp, expired_timestamp, current) FROM stdin;
\.


--
-- Data for Name: group_revision; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.group_revision (id, name, title, description, created, state, revision_id, continuity_id, expired_id, revision_timestamp, expired_timestamp, current, type, approval_status, image_url, is_organization) FROM stdin;
eb657e16-c5a5-409c-8d85-74712da3918a	kesenian	Kesenian		2019-11-04 16:44:48.977448	active	05e4fbf1-95b5-4267-93c5-38b53c38d250	eb657e16-c5a5-409c-8d85-74712da3918a	\N	2019-11-04 09:44:48.9638	9999-12-31 00:00:00	\N	group	approved	2019-11-04-094448.96051520141223-193733.151747iconsart.png	f
ce621bdc-853b-4c28-af24-f8203f4ea18b	basemap	Basemap		2019-11-04 16:54:48.365423	active	618b8421-786d-42e4-b197-a804909221c8	ce621bdc-853b-4c28-af24-f8203f4ea18b	\N	2019-11-04 09:54:48.336428	2019-11-04 09:56:18.409343	\N	group	approved	2019-11-04-095448.33092720180402-210608.839199basemap.png	f
ce621bdc-853b-4c28-af24-f8203f4ea18b	basemap	Basemap		2019-11-04 16:54:48.365423	active	410d242b-eeea-4e58-ad95-9972a93fd09d	ce621bdc-853b-4c28-af24-f8203f4ea18b	\N	2019-11-04 09:56:18.409343	9999-12-31 00:00:00	\N	group	approved	2019-11-04-095618.40569220180402-210608.839199basemap.png	f
cb7600e5-0667-4eed-8519-636a768b832f	boundaries	Boundaries		2019-11-04 16:56:47.873358	active	78b16dcd-0a9a-4210-add2-755ba6ad717d	cb7600e5-0667-4eed-8519-636a768b832f	\N	2019-11-04 09:56:47.850647	9999-12-31 00:00:00	\N	group	approved	2019-11-04-095647.84197720180402-210631.609197boundaries.png	f
ac1dc7a8-7fe6-4ebc-b6b9-41b38845d7f7	finansial	Finansial		2019-11-04 16:57:03.382296	active	6057c8b7-18a0-481d-bd88-214fe3fbbba5	ac1dc7a8-7fe6-4ebc-b6b9-41b38845d7f7	\N	2019-11-04 09:57:03.374347	9999-12-31 00:00:00	\N	group	approved	2019-11-04-095703.37248020141223-193756.990691iconsbudget.png	f
3bada3b5-c6b2-47c9-ac89-f5eaa2232804	ekonomi	Ekonomi		2019-11-04 16:57:19.106237	active	ea82b0e2-d4ec-44f3-b7c4-62cbdadeb1af	3bada3b5-c6b2-47c9-ac89-f5eaa2232804	\N	2019-11-04 09:57:19.096174	9999-12-31 00:00:00	\N	group	approved	2019-11-04-095719.09218320141223-193813.665606iconseconomy.png	f
2f4171f4-545d-4ad5-a0a0-d4aa5745fe68	pendidikan	Pendidikan		2019-11-04 16:57:32.167765	active	41251724-13bb-4dd0-9693-241e3bab4ecd	2f4171f4-545d-4ad5-a0a0-d4aa5745fe68	\N	2019-11-04 09:57:32.159988	9999-12-31 00:00:00	\N	group	approved	2019-11-04-095732.15739120141223-194239.846305iconseducation.png	f
e1024296-5068-4f29-bcd8-d137fb6cde61	politik	Politik		2019-11-04 16:57:45.68575	active	77326290-28b7-44d1-bc54-17d5c6941d4b	e1024296-5068-4f29-bcd8-d137fb6cde61	\N	2019-11-04 09:57:45.673815	9999-12-31 00:00:00	\N	group	approved	2019-11-04-095745.67095620141223-193842.916844iconselections.png	f
2c0f9867-57a3-49d4-a672-fbf9beb17bae	lingkungan-hidup	Lingkungan Hidup		2019-11-04 16:58:08.353549	active	f6c82422-3d68-4ea7-98a7-1acd83e78da6	2c0f9867-57a3-49d4-a672-fbf9beb17bae	\N	2019-11-04 09:58:08.342673	9999-12-31 00:00:00	\N	group	approved	2019-11-04-095808.34064820141223-194254.915092iconsenvironment.png	f
fb0c05e9-d9ce-462e-8c4f-e2ad911f41d8	pangan	Pangan		2019-11-04 16:59:01.245702	active	8a902bd1-eba7-47b1-80ab-6427d26f1e35	fb0c05e9-d9ce-462e-8c4f-e2ad911f41d8	\N	2019-11-04 09:59:01.235195	9999-12-31 00:00:00	\N	group	approved	2019-11-04-095901.23179620141223-194334.639060iconsfood.png	f
2cd00e37-12b3-4e09-a4d5-00b1a2c3c68d	kesehatan	Kesehatan		2019-11-04 16:59:15.945008	active	09909a8f-ab3a-4258-bf11-8612b3043289	2cd00e37-12b3-4e09-a4d5-00b1a2c3c68d	\N	2019-11-04 09:59:15.933897	9999-12-31 00:00:00	\N	group	approved	2019-11-04-095915.93053820141223-193813.665606iconseconomy.png	f
fc6edd36-7312-47e8-81e6-b149724ca8f2	pertamanan	Pertamanan		2019-11-04 16:59:51.179572	active	2faf179c-a5c4-4bd3-bfcb-007f3cf80ef9	fc6edd36-7312-47e8-81e6-b149724ca8f2	\N	2019-11-04 09:59:51.170648	9999-12-31 00:00:00	\N	group	approved	2019-11-04-095951.16839620141223-194351.335866iconsparks.png	f
3b10736c-2100-4d15-a5ad-0bd35b2871fd	tata-ruang	Tata Ruang		2019-11-04 17:00:11.237771	active	7e785b9b-f384-4995-b7d6-e4dd15d2e144	3b10736c-2100-4d15-a5ad-0bd35b2871fd	\N	2019-11-04 10:00:11.223817	9999-12-31 00:00:00	\N	group	approved	2019-11-04-100011.22152820141223-194423.154603iconsplanning.png	f
09818b3a-567b-402a-8374-e52053beff27	hukum	Hukum		2019-11-04 17:00:29.8501	active	b75a4b6e-f830-48a8-a589-02e963ce7756	09818b3a-567b-402a-8374-e52053beff27	\N	2019-11-04 10:00:29.839912	9999-12-31 00:00:00	\N	group	approved	2019-11-04-100029.83763820141223-194440.888468iconspublicsafety.png	f
b27694bb-adce-4643-81a0-d956498b8fae	industri	Industri		2019-11-04 17:00:46.369069	active	9a175ac9-4216-45ff-b983-ca720c94694c	b27694bb-adce-4643-81a0-d956498b8fae	\N	2019-11-04 10:00:46.359421	9999-12-31 00:00:00	\N	group	approved	2019-11-04-100046.35703220141223-194406.761461iconsrealestate.png	f
3fc4e7fb-ac98-4466-b99b-c0d4864aa6f9	transportasi	Transportasi		2019-11-04 17:01:13.242429	active	79e40da0-ae4e-4d0f-acca-ce3bc25265da	3fc4e7fb-ac98-4466-b99b-c0d4864aa6f9	\N	2019-11-04 10:01:13.233051	9999-12-31 00:00:00	\N	group	approved	2019-11-04-100113.22880920141223-194455.342914iconstransportation.png	f
80238a3e-9931-4e85-af8c-409e5548bafd	lainnya	Lainnya		2019-11-04 17:01:26.225478	active	18c01c9d-ddb3-4896-8c84-05f044b3f323	80238a3e-9931-4e85-af8c-409e5548bafd	\N	2019-11-04 10:01:26.203921	9999-12-31 00:00:00	\N	group	approved	2019-11-04-100126.19773520141223-194512.006490iconsdocument.png	f
\.


--
-- Data for Name: member; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.member (id, table_id, group_id, state, revision_id, table_name, capacity) FROM stdin;
35dcf1fd-40b5-4617-959d-e89654a25698	ea6cae7f-3fac-4d7b-b59b-64df33a71528	eb657e16-c5a5-409c-8d85-74712da3918a	active	05e4fbf1-95b5-4267-93c5-38b53c38d250	user	admin
1861fe94-1b81-4f19-ab35-a1618fe0cd02	ea6cae7f-3fac-4d7b-b59b-64df33a71528	ce621bdc-853b-4c28-af24-f8203f4ea18b	active	618b8421-786d-42e4-b197-a804909221c8	user	admin
c426c9d8-9e27-4291-aa48-06e332e22af4	ea6cae7f-3fac-4d7b-b59b-64df33a71528	cb7600e5-0667-4eed-8519-636a768b832f	active	78b16dcd-0a9a-4210-add2-755ba6ad717d	user	admin
5bc1e47d-9e67-431c-8d6f-092b2b92e79e	ea6cae7f-3fac-4d7b-b59b-64df33a71528	ac1dc7a8-7fe6-4ebc-b6b9-41b38845d7f7	active	6057c8b7-18a0-481d-bd88-214fe3fbbba5	user	admin
72281497-3a4c-4be6-8f2e-b32c02472016	ea6cae7f-3fac-4d7b-b59b-64df33a71528	3bada3b5-c6b2-47c9-ac89-f5eaa2232804	active	ea82b0e2-d4ec-44f3-b7c4-62cbdadeb1af	user	admin
8f9c1b2b-206e-4d05-8bab-b2c55a959c04	ea6cae7f-3fac-4d7b-b59b-64df33a71528	2f4171f4-545d-4ad5-a0a0-d4aa5745fe68	active	41251724-13bb-4dd0-9693-241e3bab4ecd	user	admin
eb8c8a2d-d373-4308-8a03-686bb9fc307f	ea6cae7f-3fac-4d7b-b59b-64df33a71528	e1024296-5068-4f29-bcd8-d137fb6cde61	active	77326290-28b7-44d1-bc54-17d5c6941d4b	user	admin
de150c60-c45f-4ccc-9074-ef2ffb9c7aaa	ea6cae7f-3fac-4d7b-b59b-64df33a71528	2c0f9867-57a3-49d4-a672-fbf9beb17bae	active	f6c82422-3d68-4ea7-98a7-1acd83e78da6	user	admin
3a5aabef-5b6f-4a15-97f2-d1885aaff83a	ea6cae7f-3fac-4d7b-b59b-64df33a71528	fb0c05e9-d9ce-462e-8c4f-e2ad911f41d8	active	8a902bd1-eba7-47b1-80ab-6427d26f1e35	user	admin
cd9c74f2-e5b4-4096-82ef-be2cf91db158	ea6cae7f-3fac-4d7b-b59b-64df33a71528	2cd00e37-12b3-4e09-a4d5-00b1a2c3c68d	active	09909a8f-ab3a-4258-bf11-8612b3043289	user	admin
c6cdde55-ea5e-4e3a-a8fd-1ba692fad7a7	ea6cae7f-3fac-4d7b-b59b-64df33a71528	fc6edd36-7312-47e8-81e6-b149724ca8f2	active	2faf179c-a5c4-4bd3-bfcb-007f3cf80ef9	user	admin
9f617bf1-9faa-4579-80bf-e80ec3183db5	ea6cae7f-3fac-4d7b-b59b-64df33a71528	3b10736c-2100-4d15-a5ad-0bd35b2871fd	active	7e785b9b-f384-4995-b7d6-e4dd15d2e144	user	admin
835f9532-c186-48eb-90d5-1d8d418b642a	ea6cae7f-3fac-4d7b-b59b-64df33a71528	09818b3a-567b-402a-8374-e52053beff27	active	b75a4b6e-f830-48a8-a589-02e963ce7756	user	admin
f7028d14-87cd-4ffd-b25d-d18ce7b47540	ea6cae7f-3fac-4d7b-b59b-64df33a71528	b27694bb-adce-4643-81a0-d956498b8fae	active	9a175ac9-4216-45ff-b983-ca720c94694c	user	admin
9a516705-34e6-412d-bd89-406ff64d8f85	ea6cae7f-3fac-4d7b-b59b-64df33a71528	3fc4e7fb-ac98-4466-b99b-c0d4864aa6f9	active	79e40da0-ae4e-4d0f-acca-ce3bc25265da	user	admin
eb6c33ad-7d31-4186-bf00-de68249f6c53	ea6cae7f-3fac-4d7b-b59b-64df33a71528	80238a3e-9931-4e85-af8c-409e5548bafd	active	18c01c9d-ddb3-4896-8c84-05f044b3f323	user	admin
\.


--
-- Data for Name: member_revision; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.member_revision (id, table_id, group_id, state, revision_id, continuity_id, expired_id, revision_timestamp, expired_timestamp, current, table_name, capacity) FROM stdin;
35dcf1fd-40b5-4617-959d-e89654a25698	ea6cae7f-3fac-4d7b-b59b-64df33a71528	eb657e16-c5a5-409c-8d85-74712da3918a	active	05e4fbf1-95b5-4267-93c5-38b53c38d250	35dcf1fd-40b5-4617-959d-e89654a25698	\N	2019-11-04 09:44:48.9638	9999-12-31 00:00:00	\N	user	admin
1861fe94-1b81-4f19-ab35-a1618fe0cd02	ea6cae7f-3fac-4d7b-b59b-64df33a71528	ce621bdc-853b-4c28-af24-f8203f4ea18b	active	618b8421-786d-42e4-b197-a804909221c8	1861fe94-1b81-4f19-ab35-a1618fe0cd02	\N	2019-11-04 09:54:48.336428	9999-12-31 00:00:00	\N	user	admin
c426c9d8-9e27-4291-aa48-06e332e22af4	ea6cae7f-3fac-4d7b-b59b-64df33a71528	cb7600e5-0667-4eed-8519-636a768b832f	active	78b16dcd-0a9a-4210-add2-755ba6ad717d	c426c9d8-9e27-4291-aa48-06e332e22af4	\N	2019-11-04 09:56:47.850647	9999-12-31 00:00:00	\N	user	admin
5bc1e47d-9e67-431c-8d6f-092b2b92e79e	ea6cae7f-3fac-4d7b-b59b-64df33a71528	ac1dc7a8-7fe6-4ebc-b6b9-41b38845d7f7	active	6057c8b7-18a0-481d-bd88-214fe3fbbba5	5bc1e47d-9e67-431c-8d6f-092b2b92e79e	\N	2019-11-04 09:57:03.374347	9999-12-31 00:00:00	\N	user	admin
72281497-3a4c-4be6-8f2e-b32c02472016	ea6cae7f-3fac-4d7b-b59b-64df33a71528	3bada3b5-c6b2-47c9-ac89-f5eaa2232804	active	ea82b0e2-d4ec-44f3-b7c4-62cbdadeb1af	72281497-3a4c-4be6-8f2e-b32c02472016	\N	2019-11-04 09:57:19.096174	9999-12-31 00:00:00	\N	user	admin
8f9c1b2b-206e-4d05-8bab-b2c55a959c04	ea6cae7f-3fac-4d7b-b59b-64df33a71528	2f4171f4-545d-4ad5-a0a0-d4aa5745fe68	active	41251724-13bb-4dd0-9693-241e3bab4ecd	8f9c1b2b-206e-4d05-8bab-b2c55a959c04	\N	2019-11-04 09:57:32.159988	9999-12-31 00:00:00	\N	user	admin
eb8c8a2d-d373-4308-8a03-686bb9fc307f	ea6cae7f-3fac-4d7b-b59b-64df33a71528	e1024296-5068-4f29-bcd8-d137fb6cde61	active	77326290-28b7-44d1-bc54-17d5c6941d4b	eb8c8a2d-d373-4308-8a03-686bb9fc307f	\N	2019-11-04 09:57:45.673815	9999-12-31 00:00:00	\N	user	admin
de150c60-c45f-4ccc-9074-ef2ffb9c7aaa	ea6cae7f-3fac-4d7b-b59b-64df33a71528	2c0f9867-57a3-49d4-a672-fbf9beb17bae	active	f6c82422-3d68-4ea7-98a7-1acd83e78da6	de150c60-c45f-4ccc-9074-ef2ffb9c7aaa	\N	2019-11-04 09:58:08.342673	9999-12-31 00:00:00	\N	user	admin
3a5aabef-5b6f-4a15-97f2-d1885aaff83a	ea6cae7f-3fac-4d7b-b59b-64df33a71528	fb0c05e9-d9ce-462e-8c4f-e2ad911f41d8	active	8a902bd1-eba7-47b1-80ab-6427d26f1e35	3a5aabef-5b6f-4a15-97f2-d1885aaff83a	\N	2019-11-04 09:59:01.235195	9999-12-31 00:00:00	\N	user	admin
cd9c74f2-e5b4-4096-82ef-be2cf91db158	ea6cae7f-3fac-4d7b-b59b-64df33a71528	2cd00e37-12b3-4e09-a4d5-00b1a2c3c68d	active	09909a8f-ab3a-4258-bf11-8612b3043289	cd9c74f2-e5b4-4096-82ef-be2cf91db158	\N	2019-11-04 09:59:15.933897	9999-12-31 00:00:00	\N	user	admin
c6cdde55-ea5e-4e3a-a8fd-1ba692fad7a7	ea6cae7f-3fac-4d7b-b59b-64df33a71528	fc6edd36-7312-47e8-81e6-b149724ca8f2	active	2faf179c-a5c4-4bd3-bfcb-007f3cf80ef9	c6cdde55-ea5e-4e3a-a8fd-1ba692fad7a7	\N	2019-11-04 09:59:51.170648	9999-12-31 00:00:00	\N	user	admin
9f617bf1-9faa-4579-80bf-e80ec3183db5	ea6cae7f-3fac-4d7b-b59b-64df33a71528	3b10736c-2100-4d15-a5ad-0bd35b2871fd	active	7e785b9b-f384-4995-b7d6-e4dd15d2e144	9f617bf1-9faa-4579-80bf-e80ec3183db5	\N	2019-11-04 10:00:11.223817	9999-12-31 00:00:00	\N	user	admin
835f9532-c186-48eb-90d5-1d8d418b642a	ea6cae7f-3fac-4d7b-b59b-64df33a71528	09818b3a-567b-402a-8374-e52053beff27	active	b75a4b6e-f830-48a8-a589-02e963ce7756	835f9532-c186-48eb-90d5-1d8d418b642a	\N	2019-11-04 10:00:29.839912	9999-12-31 00:00:00	\N	user	admin
f7028d14-87cd-4ffd-b25d-d18ce7b47540	ea6cae7f-3fac-4d7b-b59b-64df33a71528	b27694bb-adce-4643-81a0-d956498b8fae	active	9a175ac9-4216-45ff-b983-ca720c94694c	f7028d14-87cd-4ffd-b25d-d18ce7b47540	\N	2019-11-04 10:00:46.359421	9999-12-31 00:00:00	\N	user	admin
9a516705-34e6-412d-bd89-406ff64d8f85	ea6cae7f-3fac-4d7b-b59b-64df33a71528	3fc4e7fb-ac98-4466-b99b-c0d4864aa6f9	active	79e40da0-ae4e-4d0f-acca-ce3bc25265da	9a516705-34e6-412d-bd89-406ff64d8f85	\N	2019-11-04 10:01:13.233051	9999-12-31 00:00:00	\N	user	admin
eb6c33ad-7d31-4186-bf00-de68249f6c53	ea6cae7f-3fac-4d7b-b59b-64df33a71528	80238a3e-9931-4e85-af8c-409e5548bafd	active	18c01c9d-ddb3-4896-8c84-05f044b3f323	eb6c33ad-7d31-4186-bf00-de68249f6c53	\N	2019-11-04 10:01:26.203921	9999-12-31 00:00:00	\N	user	admin
\.


--
-- Data for Name: migrate_version; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.migrate_version (repository_id, repository_path, version) FROM stdin;
Ckan	/mnt/d/SERVER/PythonProject/JDS/OPENDATA/ckan-template/ckan/migration	86
\.


--
-- Data for Name: package; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.package (id, name, title, version, url, notes, license_id, revision_id, author, author_email, maintainer, maintainer_email, state, type, owner_org, private, metadata_modified, creator_user_id, metadata_created) FROM stdin;
\.


--
-- Data for Name: package_extra; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.package_extra (id, package_id, key, value, revision_id, state) FROM stdin;
\.


--
-- Data for Name: package_extra_revision; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.package_extra_revision (id, package_id, key, value, revision_id, continuity_id, state, expired_id, revision_timestamp, expired_timestamp, current) FROM stdin;
\.


--
-- Data for Name: package_relationship; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.package_relationship (id, subject_package_id, object_package_id, type, comment, revision_id, state) FROM stdin;
\.


--
-- Data for Name: package_relationship_revision; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.package_relationship_revision (id, subject_package_id, object_package_id, type, comment, revision_id, continuity_id, state, expired_id, revision_timestamp, expired_timestamp, current) FROM stdin;
\.


--
-- Data for Name: package_revision; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.package_revision (id, name, title, version, url, notes, license_id, revision_id, continuity_id, author, author_email, maintainer, maintainer_email, state, expired_id, revision_timestamp, expired_timestamp, current, type, owner_org, private, metadata_modified, creator_user_id, metadata_created) FROM stdin;
\.


--
-- Data for Name: package_tag; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.package_tag (id, package_id, tag_id, revision_id, state) FROM stdin;
\.


--
-- Data for Name: package_tag_revision; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.package_tag_revision (id, package_id, tag_id, revision_id, continuity_id, state, expired_id, revision_timestamp, expired_timestamp, current) FROM stdin;
\.


--
-- Data for Name: rating; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.rating (id, user_id, user_ip_address, package_id, rating, created) FROM stdin;
\.


--
-- Data for Name: resource; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.resource (id, url, format, description, "position", revision_id, hash, state, extras, name, resource_type, mimetype, mimetype_inner, size, last_modified, cache_url, cache_last_updated, webstore_url, webstore_last_updated, created, url_type, package_id) FROM stdin;
\.


--
-- Data for Name: resource_revision; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.resource_revision (id, url, format, description, "position", revision_id, continuity_id, hash, state, extras, expired_id, revision_timestamp, expired_timestamp, current, name, resource_type, mimetype, mimetype_inner, size, last_modified, cache_url, cache_last_updated, webstore_url, webstore_last_updated, created, url_type, package_id) FROM stdin;
\.


--
-- Data for Name: resource_view; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.resource_view (id, resource_id, title, description, view_type, "order", config) FROM stdin;
\.


--
-- Data for Name: revision; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.revision (id, "timestamp", author, message, state, approved_timestamp) FROM stdin;
d0c793d4-c412-4632-8bfa-b98f4cb34832	2019-11-04 09:07:18.877158	system	Add versioning to groups, group_extras and package_groups	active	2019-11-04 09:07:18.877158
09ef4219-fec8-45e3-93bf-7dadeaef5256	2019-11-04 09:07:19.378703	admin	Admin: make sure every object has a row in a revision table	active	2019-11-04 09:07:19.378703
05e4fbf1-95b5-4267-93c5-38b53c38d250	2019-11-04 09:44:48.9638	admin		active	\N
766e179a-17af-4249-a2d3-8bfd7fd99cb3	2019-11-04 09:44:49.021847	admin	REST API: Create member object 	active	\N
618b8421-786d-42e4-b197-a804909221c8	2019-11-04 09:54:48.336428	admin		active	\N
aa0c5572-b149-4d96-a6ff-a8d20162852f	2019-11-04 09:54:48.411173	admin	REST API: Create member object 	active	\N
410d242b-eeea-4e58-ad95-9972a93fd09d	2019-11-04 09:56:18.409343	admin		active	\N
78b16dcd-0a9a-4210-add2-755ba6ad717d	2019-11-04 09:56:47.850647	admin		active	\N
c84dc884-0d70-49a5-a72b-c6f9f4168d60	2019-11-04 09:56:47.909323	admin	REST API: Create member object 	active	\N
6057c8b7-18a0-481d-bd88-214fe3fbbba5	2019-11-04 09:57:03.374347	admin		active	\N
a065e379-3327-46a6-bda0-7cafbfa78f9d	2019-11-04 09:57:03.410818	admin	REST API: Create member object 	active	\N
ea82b0e2-d4ec-44f3-b7c4-62cbdadeb1af	2019-11-04 09:57:19.096174	admin		active	\N
959f1182-7c49-463c-9383-c2a67420f9d8	2019-11-04 09:57:19.139547	admin	REST API: Create member object 	active	\N
41251724-13bb-4dd0-9693-241e3bab4ecd	2019-11-04 09:57:32.159988	admin		active	\N
bb5d94ec-dd8f-4026-9a6f-2274fa8087ab	2019-11-04 09:57:32.195263	admin	REST API: Create member object 	active	\N
77326290-28b7-44d1-bc54-17d5c6941d4b	2019-11-04 09:57:45.673815	admin		active	\N
e338f0f6-b116-47ad-a083-99bb9f47aead	2019-11-04 09:57:45.763633	admin	REST API: Create member object 	active	\N
f6c82422-3d68-4ea7-98a7-1acd83e78da6	2019-11-04 09:58:08.342673	admin		active	\N
bfd0ad3f-0bec-4c0b-84bb-dba188322a16	2019-11-04 09:58:08.380541	admin	REST API: Create member object 	active	\N
8a902bd1-eba7-47b1-80ab-6427d26f1e35	2019-11-04 09:59:01.235195	admin		active	\N
be5b673c-e97b-47e1-aa28-840a2508f639	2019-11-04 09:59:01.276247	admin	REST API: Create member object 	active	\N
09909a8f-ab3a-4258-bf11-8612b3043289	2019-11-04 09:59:15.933897	admin		active	\N
95fb03a1-e685-4ead-86a8-5ff74c854617	2019-11-04 09:59:15.974815	admin	REST API: Create member object 	active	\N
2faf179c-a5c4-4bd3-bfcb-007f3cf80ef9	2019-11-04 09:59:51.170648	admin		active	\N
fe1e6f26-1335-439a-b024-df9ac9a6610a	2019-11-04 09:59:51.206667	admin	REST API: Create member object 	active	\N
7e785b9b-f384-4995-b7d6-e4dd15d2e144	2019-11-04 10:00:11.223817	admin		active	\N
50b4a929-74f6-480b-98f7-fdd88308db1c	2019-11-04 10:00:11.289447	admin	REST API: Create member object 	active	\N
b75a4b6e-f830-48a8-a589-02e963ce7756	2019-11-04 10:00:29.839912	admin		active	\N
d94b58d0-91ec-4ad8-a487-39b6a856ee07	2019-11-04 10:00:29.875739	admin	REST API: Create member object 	active	\N
9a175ac9-4216-45ff-b983-ca720c94694c	2019-11-04 10:00:46.359421	admin		active	\N
8e8af798-a900-48d9-8bf2-fe834509e8d8	2019-11-04 10:00:46.399103	admin	REST API: Create member object 	active	\N
79e40da0-ae4e-4d0f-acca-ce3bc25265da	2019-11-04 10:01:13.233051	admin		active	\N
4c007eb1-aba9-4dac-9f07-83e8ce670d26	2019-11-04 10:01:13.268499	admin	REST API: Create member object 	active	\N
18c01c9d-ddb3-4896-8c84-05f044b3f323	2019-11-04 10:01:26.203921	admin		active	\N
f71cfa3c-86cf-40df-abdc-05b07c5f0baa	2019-11-04 10:01:26.252928	admin	REST API: Create member object 	active	\N
\.


--
-- Data for Name: system_info; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.system_info (id, key, value, revision_id, state) FROM stdin;
\.


--
-- Data for Name: system_info_revision; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.system_info_revision (id, key, value, revision_id, continuity_id, state, expired_id, revision_timestamp, expired_timestamp, current) FROM stdin;
\.


--
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.tag (id, name, vocabulary_id) FROM stdin;
\.


--
-- Data for Name: task_status; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.task_status (id, entity_id, entity_type, task_type, key, value, state, error, last_updated) FROM stdin;
\.


--
-- Data for Name: term_translation; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.term_translation (term, term_translation, lang_code) FROM stdin;
\.


--
-- Data for Name: tracking_raw; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.tracking_raw (user_key, url, tracking_type, access_timestamp) FROM stdin;
\.


--
-- Data for Name: tracking_summary; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.tracking_summary (url, package_id, tracking_type, count, running_total, recent_views, tracking_date) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public."user" (id, name, apikey, created, about, password, fullname, email, reset_key, sysadmin, activity_streams_email_notifications, state) FROM stdin;
ea6cae7f-3fac-4d7b-b59b-64df33a71528	admin	9be7bdc6-b2ff-4a6b-ad64-15f79480e4ab	2019-11-04 16:10:03.563052	\N	$pbkdf2-sha512$25000$dC6FUMq5tzYGwFjL2dv7Pw$CHmn0ZlUIvPWdZHp8xqiKG5eXouPgxGGiDEto.WdVnRKE3f4IjCKVjQt3G2DS7mxPrvBWLxsYBo5En.gZw279A	\N	admin@email.com	\N	t	f	active
\.


--
-- Data for Name: user_following_dataset; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.user_following_dataset (follower_id, object_id, datetime) FROM stdin;
\.


--
-- Data for Name: user_following_group; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.user_following_group (follower_id, object_id, datetime) FROM stdin;
\.


--
-- Data for Name: user_following_user; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.user_following_user (follower_id, object_id, datetime) FROM stdin;
\.


--
-- Data for Name: vocabulary; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.vocabulary (id, name) FROM stdin;
\.


--
-- Name: system_info_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.system_info_id_seq', 1, false);


--
-- Name: activity_detail activity_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.activity_detail
    ADD CONSTRAINT activity_detail_pkey PRIMARY KEY (id);


--
-- Name: activity activity_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.activity
    ADD CONSTRAINT activity_pkey PRIMARY KEY (id);


--
-- Name: authorization_group authorization_group_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authorization_group
    ADD CONSTRAINT authorization_group_pkey PRIMARY KEY (id);


--
-- Name: authorization_group_user authorization_group_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authorization_group_user
    ADD CONSTRAINT authorization_group_user_pkey PRIMARY KEY (id);


--
-- Name: dashboard dashboard_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dashboard
    ADD CONSTRAINT dashboard_pkey PRIMARY KEY (user_id);


--
-- Name: group_extra group_extra_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.group_extra
    ADD CONSTRAINT group_extra_pkey PRIMARY KEY (id);


--
-- Name: group_extra_revision group_extra_revision_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.group_extra_revision
    ADD CONSTRAINT group_extra_revision_pkey PRIMARY KEY (id, revision_id);


--
-- Name: group group_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."group"
    ADD CONSTRAINT group_name_key UNIQUE (name);


--
-- Name: group group_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."group"
    ADD CONSTRAINT group_pkey PRIMARY KEY (id);


--
-- Name: group_revision group_revision_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.group_revision
    ADD CONSTRAINT group_revision_pkey PRIMARY KEY (id, revision_id);


--
-- Name: member member_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.member
    ADD CONSTRAINT member_pkey PRIMARY KEY (id);


--
-- Name: member_revision member_revision_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.member_revision
    ADD CONSTRAINT member_revision_pkey PRIMARY KEY (id, revision_id);


--
-- Name: migrate_version migrate_version_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.migrate_version
    ADD CONSTRAINT migrate_version_pkey PRIMARY KEY (repository_id);


--
-- Name: package_extra package_extra_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_extra
    ADD CONSTRAINT package_extra_pkey PRIMARY KEY (id);


--
-- Name: package_extra_revision package_extra_revision_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_extra_revision
    ADD CONSTRAINT package_extra_revision_pkey PRIMARY KEY (id, revision_id);


--
-- Name: package package_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package
    ADD CONSTRAINT package_name_key UNIQUE (name);


--
-- Name: package package_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package
    ADD CONSTRAINT package_pkey PRIMARY KEY (id);


--
-- Name: package_relationship package_relationship_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_relationship
    ADD CONSTRAINT package_relationship_pkey PRIMARY KEY (id);


--
-- Name: package_relationship_revision package_relationship_revision_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_relationship_revision
    ADD CONSTRAINT package_relationship_revision_pkey PRIMARY KEY (id, revision_id);


--
-- Name: package_revision package_revision_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_revision
    ADD CONSTRAINT package_revision_pkey PRIMARY KEY (id, revision_id);


--
-- Name: package_tag package_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_tag
    ADD CONSTRAINT package_tag_pkey PRIMARY KEY (id);


--
-- Name: package_tag_revision package_tag_revision_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_tag_revision
    ADD CONSTRAINT package_tag_revision_pkey PRIMARY KEY (id, revision_id);


--
-- Name: rating rating_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rating
    ADD CONSTRAINT rating_pkey PRIMARY KEY (id);


--
-- Name: resource resource_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource
    ADD CONSTRAINT resource_pkey PRIMARY KEY (id);


--
-- Name: resource_revision resource_revision_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_revision
    ADD CONSTRAINT resource_revision_pkey PRIMARY KEY (id, revision_id);


--
-- Name: resource_view resource_view_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_view
    ADD CONSTRAINT resource_view_pkey PRIMARY KEY (id);


--
-- Name: revision revision_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.revision
    ADD CONSTRAINT revision_pkey PRIMARY KEY (id);


--
-- Name: system_info system_info_key_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_info
    ADD CONSTRAINT system_info_key_key UNIQUE (key);


--
-- Name: system_info system_info_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_info
    ADD CONSTRAINT system_info_pkey PRIMARY KEY (id);


--
-- Name: system_info_revision system_info_revision_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_info_revision
    ADD CONSTRAINT system_info_revision_pkey PRIMARY KEY (id, revision_id);


--
-- Name: tag tag_name_vocabulary_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT tag_name_vocabulary_id_key UNIQUE (name, vocabulary_id);


--
-- Name: tag tag_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (id);


--
-- Name: task_status task_status_entity_id_task_type_key_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.task_status
    ADD CONSTRAINT task_status_entity_id_task_type_key_key UNIQUE (entity_id, task_type, key);


--
-- Name: task_status task_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.task_status
    ADD CONSTRAINT task_status_pkey PRIMARY KEY (id);


--
-- Name: user_following_dataset user_following_dataset_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_following_dataset
    ADD CONSTRAINT user_following_dataset_pkey PRIMARY KEY (follower_id, object_id);


--
-- Name: user_following_group user_following_group_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_following_group
    ADD CONSTRAINT user_following_group_pkey PRIMARY KEY (follower_id, object_id);


--
-- Name: user_following_user user_following_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_following_user
    ADD CONSTRAINT user_following_user_pkey PRIMARY KEY (follower_id, object_id);


--
-- Name: user user_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_name_key UNIQUE (name);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: vocabulary vocabulary_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vocabulary
    ADD CONSTRAINT vocabulary_name_key UNIQUE (name);


--
-- Name: vocabulary vocabulary_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vocabulary
    ADD CONSTRAINT vocabulary_pkey PRIMARY KEY (id);


--
-- Name: idx_activity_detail_activity_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_activity_detail_activity_id ON public.activity_detail USING btree (activity_id);


--
-- Name: idx_activity_object_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_activity_object_id ON public.activity USING btree (object_id, "timestamp");


--
-- Name: idx_activity_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_activity_user_id ON public.activity USING btree (user_id, "timestamp");


--
-- Name: idx_extra_grp_id_pkg_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_extra_grp_id_pkg_id ON public.member USING btree (group_id, table_id);


--
-- Name: idx_extra_id_pkg_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_extra_id_pkg_id ON public.package_extra USING btree (id, package_id);


--
-- Name: idx_extra_pkg_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_extra_pkg_id ON public.package_extra USING btree (package_id);


--
-- Name: idx_group_current; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_group_current ON public.group_revision USING btree (current);


--
-- Name: idx_group_extra_current; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_group_extra_current ON public.group_extra_revision USING btree (current);


--
-- Name: idx_group_extra_period; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_group_extra_period ON public.group_extra_revision USING btree (revision_timestamp, expired_timestamp, id);


--
-- Name: idx_group_extra_period_group; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_group_extra_period_group ON public.group_extra_revision USING btree (revision_timestamp, expired_timestamp, group_id);


--
-- Name: idx_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_group_id ON public."group" USING btree (id);


--
-- Name: idx_group_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_group_name ON public."group" USING btree (name);


--
-- Name: idx_group_period; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_group_period ON public.group_revision USING btree (revision_timestamp, expired_timestamp, id);


--
-- Name: idx_group_pkg_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_group_pkg_id ON public.member USING btree (table_id);


--
-- Name: idx_member_continuity_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_member_continuity_id ON public.member_revision USING btree (continuity_id);


--
-- Name: idx_package_continuity_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_continuity_id ON public.package_revision USING btree (continuity_id);


--
-- Name: idx_package_creator_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_creator_user_id ON public.package USING btree (creator_user_id);


--
-- Name: idx_package_current; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_current ON public.package_revision USING btree (current);


--
-- Name: idx_package_extra_continuity_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_extra_continuity_id ON public.package_extra_revision USING btree (continuity_id);


--
-- Name: idx_package_extra_current; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_extra_current ON public.package_extra_revision USING btree (current);


--
-- Name: idx_package_extra_package_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_extra_package_id ON public.package_extra_revision USING btree (package_id, current);


--
-- Name: idx_package_extra_period; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_extra_period ON public.package_extra_revision USING btree (revision_timestamp, expired_timestamp, id);


--
-- Name: idx_package_extra_period_package; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_extra_period_package ON public.package_extra_revision USING btree (revision_timestamp, expired_timestamp, package_id);


--
-- Name: idx_package_extra_rev_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_extra_rev_id ON public.package_extra_revision USING btree (revision_id);


--
-- Name: idx_package_group_current; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_group_current ON public.member_revision USING btree (current);


--
-- Name: idx_package_group_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_group_group_id ON public.member USING btree (group_id);


--
-- Name: idx_package_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_group_id ON public.member USING btree (id);


--
-- Name: idx_package_group_period_package_group; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_group_period_package_group ON public.member_revision USING btree (revision_timestamp, expired_timestamp, table_id, group_id);


--
-- Name: idx_package_group_pkg_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_group_pkg_id ON public.member USING btree (table_id);


--
-- Name: idx_package_group_pkg_id_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_group_pkg_id_group_id ON public.member USING btree (group_id, table_id);


--
-- Name: idx_package_period; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_period ON public.package_revision USING btree (revision_timestamp, expired_timestamp, id);


--
-- Name: idx_package_relationship_current; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_relationship_current ON public.package_relationship_revision USING btree (current);


--
-- Name: idx_package_resource_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_resource_id ON public.resource USING btree (id);


--
-- Name: idx_package_resource_rev_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_resource_rev_id ON public.resource_revision USING btree (revision_id);


--
-- Name: idx_package_resource_url; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_resource_url ON public.resource USING btree (url);


--
-- Name: idx_package_tag_continuity_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_tag_continuity_id ON public.package_tag_revision USING btree (continuity_id);


--
-- Name: idx_package_tag_current; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_tag_current ON public.package_tag_revision USING btree (current);


--
-- Name: idx_package_tag_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_tag_id ON public.package_tag USING btree (id);


--
-- Name: idx_package_tag_pkg_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_tag_pkg_id ON public.package_tag USING btree (package_id);


--
-- Name: idx_package_tag_pkg_id_tag_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_tag_pkg_id_tag_id ON public.package_tag USING btree (tag_id, package_id);


--
-- Name: idx_package_tag_revision_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_tag_revision_id ON public.package_tag_revision USING btree (id);


--
-- Name: idx_package_tag_revision_pkg_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_tag_revision_pkg_id ON public.package_tag_revision USING btree (package_id);


--
-- Name: idx_package_tag_revision_pkg_id_tag_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_tag_revision_pkg_id_tag_id ON public.package_tag_revision USING btree (tag_id, package_id);


--
-- Name: idx_package_tag_revision_rev_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_tag_revision_rev_id ON public.package_tag_revision USING btree (revision_id);


--
-- Name: idx_package_tag_revision_tag_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_tag_revision_tag_id ON public.package_tag_revision USING btree (tag_id);


--
-- Name: idx_package_tag_tag_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_package_tag_tag_id ON public.package_tag USING btree (tag_id);


--
-- Name: idx_period_package_relationship; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_period_package_relationship ON public.package_relationship_revision USING btree (revision_timestamp, expired_timestamp, object_package_id, subject_package_id);


--
-- Name: idx_period_package_tag; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_period_package_tag ON public.package_tag_revision USING btree (revision_timestamp, expired_timestamp, package_id, tag_id);


--
-- Name: idx_pkg_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_pkg_id ON public.package USING btree (id);


--
-- Name: idx_pkg_lname; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_pkg_lname ON public.package USING btree (lower((name)::text));


--
-- Name: idx_pkg_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_pkg_name ON public.package USING btree (name);


--
-- Name: idx_pkg_rev_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_pkg_rev_id ON public.package USING btree (revision_id);


--
-- Name: idx_pkg_revision_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_pkg_revision_id ON public.package_revision USING btree (id);


--
-- Name: idx_pkg_revision_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_pkg_revision_name ON public.package_revision USING btree (name);


--
-- Name: idx_pkg_revision_rev_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_pkg_revision_rev_id ON public.package_revision USING btree (revision_id);


--
-- Name: idx_pkg_sid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_pkg_sid ON public.package USING btree (id, state);


--
-- Name: idx_pkg_slname; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_pkg_slname ON public.package USING btree (lower((name)::text), state);


--
-- Name: idx_pkg_sname; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_pkg_sname ON public.package USING btree (name, state);


--
-- Name: idx_pkg_srev_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_pkg_srev_id ON public.package USING btree (revision_id, state);


--
-- Name: idx_pkg_stitle; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_pkg_stitle ON public.package USING btree (title, state);


--
-- Name: idx_pkg_suname; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_pkg_suname ON public.package USING btree (upper((name)::text), state);


--
-- Name: idx_pkg_title; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_pkg_title ON public.package USING btree (title);


--
-- Name: idx_pkg_uname; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_pkg_uname ON public.package USING btree (upper((name)::text));


--
-- Name: idx_rating_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_rating_id ON public.rating USING btree (id);


--
-- Name: idx_rating_package_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_rating_package_id ON public.rating USING btree (package_id);


--
-- Name: idx_rating_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_rating_user_id ON public.rating USING btree (user_id);


--
-- Name: idx_resource_continuity_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_resource_continuity_id ON public.resource_revision USING btree (continuity_id);


--
-- Name: idx_resource_current; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_resource_current ON public.resource_revision USING btree (current);


--
-- Name: idx_resource_period; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_resource_period ON public.resource_revision USING btree (revision_timestamp, expired_timestamp, id);


--
-- Name: idx_rev_state; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_rev_state ON public.revision USING btree (state);


--
-- Name: idx_revision_author; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_revision_author ON public.revision USING btree (author);


--
-- Name: idx_tag_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_tag_id ON public.tag USING btree (id);


--
-- Name: idx_tag_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_tag_name ON public.tag USING btree (name);


--
-- Name: idx_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_user_id ON public."user" USING btree (id);


--
-- Name: idx_user_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_user_name ON public."user" USING btree (name);


--
-- Name: idx_user_name_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_user_name_index ON public."user" USING btree ((
CASE
    WHEN ((fullname IS NULL) OR (fullname = ''::text)) THEN name
    ELSE fullname
END));


--
-- Name: term; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX term ON public.term_translation USING btree (term);


--
-- Name: term_lang; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX term_lang ON public.term_translation USING btree (term, lang_code);


--
-- Name: tracking_raw_access_timestamp; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tracking_raw_access_timestamp ON public.tracking_raw USING btree (access_timestamp);


--
-- Name: tracking_raw_url; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tracking_raw_url ON public.tracking_raw USING btree (url);


--
-- Name: tracking_raw_user_key; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tracking_raw_user_key ON public.tracking_raw USING btree (user_key);


--
-- Name: tracking_summary_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tracking_summary_date ON public.tracking_summary USING btree (tracking_date);


--
-- Name: tracking_summary_package_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tracking_summary_package_id ON public.tracking_summary USING btree (package_id);


--
-- Name: tracking_summary_url; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tracking_summary_url ON public.tracking_summary USING btree (url);


--
-- Name: activity_detail activity_detail_activity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.activity_detail
    ADD CONSTRAINT activity_detail_activity_id_fkey FOREIGN KEY (activity_id) REFERENCES public.activity(id);


--
-- Name: authorization_group_user authorization_group_user_authorization_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authorization_group_user
    ADD CONSTRAINT authorization_group_user_authorization_group_id_fkey FOREIGN KEY (authorization_group_id) REFERENCES public.authorization_group(id);


--
-- Name: authorization_group_user authorization_group_user_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authorization_group_user
    ADD CONSTRAINT authorization_group_user_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- Name: dashboard dashboard_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dashboard
    ADD CONSTRAINT dashboard_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: group_extra group_extra_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.group_extra
    ADD CONSTRAINT group_extra_group_id_fkey FOREIGN KEY (group_id) REFERENCES public."group"(id);


--
-- Name: group_extra_revision group_extra_revision_continuity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.group_extra_revision
    ADD CONSTRAINT group_extra_revision_continuity_id_fkey FOREIGN KEY (continuity_id) REFERENCES public.group_extra(id);


--
-- Name: group_extra_revision group_extra_revision_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.group_extra_revision
    ADD CONSTRAINT group_extra_revision_group_id_fkey FOREIGN KEY (group_id) REFERENCES public."group"(id);


--
-- Name: group_extra group_extra_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.group_extra
    ADD CONSTRAINT group_extra_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: group_extra_revision group_extra_revision_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.group_extra_revision
    ADD CONSTRAINT group_extra_revision_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: group_revision group_revision_continuity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.group_revision
    ADD CONSTRAINT group_revision_continuity_id_fkey FOREIGN KEY (continuity_id) REFERENCES public."group"(id);


--
-- Name: group group_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."group"
    ADD CONSTRAINT group_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: group_revision group_revision_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.group_revision
    ADD CONSTRAINT group_revision_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: member member_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.member
    ADD CONSTRAINT member_group_id_fkey FOREIGN KEY (group_id) REFERENCES public."group"(id);


--
-- Name: member_revision member_revision_continuity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.member_revision
    ADD CONSTRAINT member_revision_continuity_id_fkey FOREIGN KEY (continuity_id) REFERENCES public.member(id);


--
-- Name: member_revision member_revision_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.member_revision
    ADD CONSTRAINT member_revision_group_id_fkey FOREIGN KEY (group_id) REFERENCES public."group"(id);


--
-- Name: member member_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.member
    ADD CONSTRAINT member_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: member_revision member_revision_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.member_revision
    ADD CONSTRAINT member_revision_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: package_extra package_extra_package_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_extra
    ADD CONSTRAINT package_extra_package_id_fkey FOREIGN KEY (package_id) REFERENCES public.package(id);


--
-- Name: package_extra_revision package_extra_revision_continuity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_extra_revision
    ADD CONSTRAINT package_extra_revision_continuity_id_fkey FOREIGN KEY (continuity_id) REFERENCES public.package_extra(id);


--
-- Name: package_extra package_extra_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_extra
    ADD CONSTRAINT package_extra_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: package_extra_revision package_extra_revision_package_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_extra_revision
    ADD CONSTRAINT package_extra_revision_package_id_fkey FOREIGN KEY (package_id) REFERENCES public.package(id);


--
-- Name: package_extra_revision package_extra_revision_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_extra_revision
    ADD CONSTRAINT package_extra_revision_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: package_relationship package_relationship_object_package_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_relationship
    ADD CONSTRAINT package_relationship_object_package_id_fkey FOREIGN KEY (object_package_id) REFERENCES public.package(id);


--
-- Name: package_relationship_revision package_relationship_revision_continuity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_relationship_revision
    ADD CONSTRAINT package_relationship_revision_continuity_id_fkey FOREIGN KEY (continuity_id) REFERENCES public.package_relationship(id);


--
-- Name: package_relationship package_relationship_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_relationship
    ADD CONSTRAINT package_relationship_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: package_relationship_revision package_relationship_revision_object_package_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_relationship_revision
    ADD CONSTRAINT package_relationship_revision_object_package_id_fkey FOREIGN KEY (object_package_id) REFERENCES public.package(id);


--
-- Name: package_relationship_revision package_relationship_revision_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_relationship_revision
    ADD CONSTRAINT package_relationship_revision_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: package_relationship_revision package_relationship_revision_subject_package_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_relationship_revision
    ADD CONSTRAINT package_relationship_revision_subject_package_id_fkey FOREIGN KEY (subject_package_id) REFERENCES public.package(id);


--
-- Name: package_relationship package_relationship_subject_package_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_relationship
    ADD CONSTRAINT package_relationship_subject_package_id_fkey FOREIGN KEY (subject_package_id) REFERENCES public.package(id);


--
-- Name: package_revision package_revision_continuity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_revision
    ADD CONSTRAINT package_revision_continuity_id_fkey FOREIGN KEY (continuity_id) REFERENCES public.package(id);


--
-- Name: package package_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package
    ADD CONSTRAINT package_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: package_revision package_revision_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_revision
    ADD CONSTRAINT package_revision_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: package_tag package_tag_package_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_tag
    ADD CONSTRAINT package_tag_package_id_fkey FOREIGN KEY (package_id) REFERENCES public.package(id);


--
-- Name: package_tag_revision package_tag_revision_continuity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_tag_revision
    ADD CONSTRAINT package_tag_revision_continuity_id_fkey FOREIGN KEY (continuity_id) REFERENCES public.package_tag(id);


--
-- Name: package_tag package_tag_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_tag
    ADD CONSTRAINT package_tag_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: package_tag_revision package_tag_revision_package_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_tag_revision
    ADD CONSTRAINT package_tag_revision_package_id_fkey FOREIGN KEY (package_id) REFERENCES public.package(id);


--
-- Name: package_tag_revision package_tag_revision_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_tag_revision
    ADD CONSTRAINT package_tag_revision_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: package_tag_revision package_tag_revision_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_tag_revision
    ADD CONSTRAINT package_tag_revision_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES public.tag(id);


--
-- Name: package_tag package_tag_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_tag
    ADD CONSTRAINT package_tag_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES public.tag(id);


--
-- Name: rating rating_package_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rating
    ADD CONSTRAINT rating_package_id_fkey FOREIGN KEY (package_id) REFERENCES public.package(id);


--
-- Name: rating rating_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rating
    ADD CONSTRAINT rating_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- Name: resource_revision resource_revision_continuity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_revision
    ADD CONSTRAINT resource_revision_continuity_id_fkey FOREIGN KEY (continuity_id) REFERENCES public.resource(id);


--
-- Name: resource resource_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource
    ADD CONSTRAINT resource_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: resource_revision resource_revision_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_revision
    ADD CONSTRAINT resource_revision_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: resource_view resource_view_resource_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.resource_view
    ADD CONSTRAINT resource_view_resource_id_fkey FOREIGN KEY (resource_id) REFERENCES public.resource(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: system_info_revision system_info_revision_continuity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_info_revision
    ADD CONSTRAINT system_info_revision_continuity_id_fkey FOREIGN KEY (continuity_id) REFERENCES public.system_info(id);


--
-- Name: system_info system_info_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_info
    ADD CONSTRAINT system_info_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: system_info_revision system_info_revision_revision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_info_revision
    ADD CONSTRAINT system_info_revision_revision_id_fkey FOREIGN KEY (revision_id) REFERENCES public.revision(id);


--
-- Name: tag tag_vocabulary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT tag_vocabulary_id_fkey FOREIGN KEY (vocabulary_id) REFERENCES public.vocabulary(id);


--
-- Name: user_following_dataset user_following_dataset_follower_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_following_dataset
    ADD CONSTRAINT user_following_dataset_follower_id_fkey FOREIGN KEY (follower_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_following_dataset user_following_dataset_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_following_dataset
    ADD CONSTRAINT user_following_dataset_object_id_fkey FOREIGN KEY (object_id) REFERENCES public.package(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_following_group user_following_group_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_following_group
    ADD CONSTRAINT user_following_group_group_id_fkey FOREIGN KEY (object_id) REFERENCES public."group"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_following_group user_following_group_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_following_group
    ADD CONSTRAINT user_following_group_user_id_fkey FOREIGN KEY (follower_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_following_user user_following_user_follower_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_following_user
    ADD CONSTRAINT user_following_user_follower_id_fkey FOREIGN KEY (follower_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_following_user user_following_user_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_following_user
    ADD CONSTRAINT user_following_user_object_id_fkey FOREIGN KEY (object_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

